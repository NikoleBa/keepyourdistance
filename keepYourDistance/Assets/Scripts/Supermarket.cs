﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class Supermarket : MonoBehaviour
    {
        public GameObject bubble;
        public GameObject player;
        public Animator animator;
        public HighScore highScore;

        //If the player arrives at the market, we disable the controlls and activate the text bubble, then change the scene
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
               animator.SetTrigger("PlayerArrived"); 
               player.GetComponent<PlayerMovement>().canMove = false;
               player.GetComponent<CharacterController2D>().canMove = false;

               bubble.gameObject.SetActive(true);
            }

        }

        public void ChangeScene()
        {
            highScore.SaveHighScore();
            SceneManager.LoadScene("Outro");
        }
        
    }
}
