﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Damage : MonoBehaviour
    {
        Color color;
        //public AudioSource oofSound;
        private GameObject player;
        private bool invulnerable = false;

        private void Awake()
        {
            player = GameObject.Find("Player");
            //we define the variable "color" as the standard sprite to manipulate it later
            color = player.GetComponent<SpriteRenderer>().material.color;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(GameTags.PLAYER) && !invulnerable)
            {//take one damage on collision
                GameEvents.current.Damaged();
            //since our player always recieves 2 dmg (by using two colliders) we set a invunerability bool
                invulnerable = true;
          //      oofSound.Play();

            //to avoid getting multiple hits on collison, we give the player a short time window on invunerabilty
                StartCoroutine("GetInvulnerable");

            }

        }
        //Coroutine in which collision layers between player and enemies get disabled for a set time
        IEnumerator GetInvulnerable()
        {
            Physics2D.IgnoreLayerCollision(9, 10, true);
            //we half the opacity/alpha channel of our defined color to visualize the invulnerabality 
            color.a = 0.5f;
            player.GetComponent<SpriteRenderer>().material.color = color;
            yield return new WaitForSeconds(1f);
            Physics2D.IgnoreLayerCollision(9, 10, false);
            //turn the opacity/alpha channel back to its normal value
            color.a = 1f;
            player.GetComponent<SpriteRenderer>().material.color = color;
            invulnerable = false;
        }
    }
}