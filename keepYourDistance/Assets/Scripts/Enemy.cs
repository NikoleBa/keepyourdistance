﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace PRTwo
{
    public class Enemy : MonoBehaviour
    {
        public int health = 3;

        // Serialized reference of the HighScore script, assign this in the Inspector
        [SerializeField]
        private int score;

        [SerializeField]
        private float speed;
        [SerializeField]
        public bool goRight;

        public GameObject destroyObject;

        [SerializeField]
        private LayerMask layerMask;
        [SerializeField]
        private Transform raycastPoint;
        

        void Update()
        {
            //Entscheidet die Richtung, in die sich der Gegner bewegt
            if (goRight == false)
            {
                transform.position += Vector3.left * speed * Time.deltaTime;
            }
            else
            {
                transform.position += Vector3.right * speed * Time.deltaTime;
            }

        }
        private void FixedUpdate()
        {
            //Sorgt dafür, dass ein Gegner die Richtung wechselt, wenn er einen bestimmten Layer berührt
            if (Physics2D.Raycast(raycastPoint.position, transform.right, 0.5f, layerMask))
            {
                if (goRight == true)
                {
                    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, -180, transform.eulerAngles.z);
                    goRight = false;
                }
                else
                {
                    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 0, transform.eulerAngles.z);
                    goRight = true;
                }
            }
        }
        //Erlaubt den Gegnern Damage zu bekommen, sobald sie getroffen werden
        public void TakeDamage(int damage)
        {
            health -= damage;

            if (health <= 0)
            {
                Die();
            }
        }
        //Deaktiviert den Collider der Damage verursacht, zerstört ein Gameobjekt und  erhöht den Highscore
        void Die()
        {
            // Der Collider hängt am selben GameObject 
            Collider2D collider = GetComponent<Collider2D>();
            //bei manchen Gegnern hängt er jedoch am child
            if (collider == null)
            {
                collider = GetComponentInChildren<Collider2D>();
            }

            collider.enabled = false;
            destroyObject.SetActive(false);
            GameEvents.current.EnemyKilled(score);
        }
    }
}
