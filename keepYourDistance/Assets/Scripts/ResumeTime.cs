﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    //We froze time in the "Game Over" scenario, so we have to reset it or another playthrough is impossible
    public class ResumeTime : MonoBehaviour
    {
        private void Awake()
        {
            Time.timeScale = 1;
        }
    }

}
