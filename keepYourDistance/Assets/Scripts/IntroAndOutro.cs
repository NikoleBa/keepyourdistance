﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class IntroAndOutro : MonoBehaviour
    {
        //a small array that hold both images for either the intro or outro scene
        public GameObject[] images;
        public string sceneName;
        private int index = 0;


        void Update()
        {
      //press any key to raise the index number=show the next image
            if(Input.anyKeyDown)
            {
                index++;
                // end of indey? no more images are left, so change the scene
                if (index >= images.Length)
                {
                  
                    SceneManager.LoadScene(sceneName);
                    return;
                }
                else
                { //if an image is left in the index, show it instead
                    images[index].SetActive(true);
                }
                
            }
        }
    }
}
