﻿using System.IO;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PRTwo
{//we inherit from HighScore to reuse its functionality
    public class HighScoreMenu : HighScore
    {
        //as a child, this script can override parent methods. we use this to return the all time highscore
        // for the menu specifically
        protected override string GetHighscoreValue()
        {
            return PlayerPrefs.GetInt("HighScore", 0).ToString();
        }
    }
}