﻿using UnityEngine.Audio;
using System;
using UnityEngine;

namespace PRTwo
{
    [SerializeField]
    public class AudioManager : MonoBehaviour
    {
        //array für sounds erstellen (für den inspector)
        public Sound[] sounds;

        //soll nach sounds suchen und dann dazu noch interfaces zum referenzieren für einen clip, dessen volume und den pitch
        void Awake()
        {
            foreach (Sound s in sounds)
            {
                s.source = gameObject.AddComponent<AudioSource>();
                s.source.clip = s.clip;

                s.source.volume = s.volume;
                s.source.pitch = s.pitch;
            }
        }
        //einen namen, um den sound abzuspielen
        public void Play(string name)
        {
            Sound s = Array.Find(sounds, sound => sound.name == name);
            s.source.Play();
        }
    }
}