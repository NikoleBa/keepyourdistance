﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class PlayerMovement : MonoBehaviour
    {
	    public CharacterController2D controller;
	    public Animator animator;
	    public float runSpeed = 40f;

	    float horizontalMove = 0f;
	    bool jump = false;
        public bool canMove = true;

        // Update is called once per frame
        void Update()
	    {
            if (!canMove)
            { animator.SetFloat("Speed",0);
                return;
            }

            horizontalMove = Input.GetAxis("Horizontal") * runSpeed;

		    animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

		    if (Input.GetButtonDown("Jump"))
		    {
			    jump = true;
			    animator.SetBool("IsJumping", true);

		    }

            if (Input.GetButtonDown("Throw"))
            {
                animator.SetTrigger("Throw");
            }
	    }

	    public void OnLanding()
	    {            
		    animator.SetBool("IsJumping", false);
       
	    }
	    void FixedUpdate()
	    {
            if (!canMove)
            {
                return;
            }
            // Move our character
            controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
		    jump = false;
	    }

      
    }
}