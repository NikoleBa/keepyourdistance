﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PRTwo
{
    public class CharacterController2D : MonoBehaviour
    {
	    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
	    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
	    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
	    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
		[SerializeField] private int consecutiveHits = 0; 
	    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
	    [SerializeField] private Transform m_CeilingCheck; // A position marking where to check for ceilings
	    [SerializeField] private int tpCounter;
	    [SerializeField] private int jumpCounter;

	    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	    private bool m_Grounded;            // Whether or not the player is grounded.
	    private Rigidbody2D m_Rigidbody2D;
	    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
	    private Vector3 m_Velocity = Vector3.zero;
        public bool canMove = true;
        private bool sniperAchievementDone = false;

	    [Header("Events")]
	    [Space]

	    public UnityEvent OnLandEvent;

	    [System.Serializable]
	    public class BoolEvent : UnityEvent<bool> { }

	    private void Awake()
	    {
		    m_Rigidbody2D = GetComponent<Rigidbody2D>();

		    if (OnLandEvent == null)
			    OnLandEvent = new UnityEvent();
	    }

	    private void Start()
	    {
		    GameEvents.current.EnemyHit += ConsecutiveHitsUp;
		    GameEvents.current.EnemyMissed += ConsecutiveHitsReset;
		    GameEvents.current.ToiletPaperCollect += TPCollectorUp;
	    }

	    private void Update()
	    {
		    if (consecutiveHits == 10 && !sniperAchievementDone)
		    {
			    GameEvents.current.ConsecutiveHits();
			    sniperAchievementDone = true;
		    }

		    if (tpCounter == 157)
		    {
			    tpCounter++;
			    GameEvents.current.TPCollector();
		    }

		    if (jumpCounter == 250)
		    {
			    jumpCounter++;
			    GameEvents.current.BunnyHopper();
		    }
	    }

	    private void TPCollectorUp()
	    {
		    tpCounter++;
	    }

	    private void FixedUpdate()
	    {
            if(!canMove)
            {
                return;
            }

		    bool wasGrounded = m_Grounded;
		    m_Grounded = false;

		    // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		    // This can be done using layers instead but Sample Assets will not overwrite your project settings.
		    Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		    for (int i = 0; i < colliders.Length; i++)
		    {
			    if (colliders[i].gameObject != gameObject)
			    {
				    m_Grounded = true;
				    if (!wasGrounded)
					    OnLandEvent.Invoke();
			    }
		    }
	    }

	    private void ConsecutiveHitsUp()
	    {
		    consecutiveHits++;
	    }

	    private void ConsecutiveHitsReset()
	    {
		    if (!sniperAchievementDone)
		    {
			    consecutiveHits = 0;
		    }
		    else
		    {
			    return;
		    }
	    }

	    public void Move(float move, bool jump)
	    {


		    //only control the player if grounded or airControl is turned on
		    if (m_Grounded || m_AirControl)
		    {

			    // Move the character by finding the target velocity
			    Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);
			    // And then smoothing it out and applying it to the character
			    m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

			    // If the input is moving the player right and the player is facing left...
			    if (move > 0 && !m_FacingRight)
			    {
				    // ... flip the player.
				    Flip();
			    }
			    // Otherwise if the input is moving the player left and the player is facing right...
			    else if (move < 0 && m_FacingRight)
			    {
				    // ... flip the player.
				    Flip();
			    }
		    }
		    // If the player should jump...
		    if (m_Grounded && jump)
		    {
			    // Add a vertical force to the player.
			    jumpCounter ++;
			    m_Grounded = false;
			    m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
		    }
	    }
	    private void Flip()
	    {
		    // Switch the way the player is labelled as facing.
		    m_FacingRight = !m_FacingRight;

		    transform.Rotate(0f, 180f, 0f);
	    }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if(other.gameObject.CompareTag("TP"))
            {
                Destroy(other.gameObject);
            }

            if (other.gameObject.CompareTag("HP"))
            {
	            GameEvents.current.HighestPoint();
	            Destroy(other.gameObject);
            }

            if (other.gameObject.CompareTag("Supermarket"))
            {
	            GameEvents.current.LoadNewLevel();
            }

            if (other.gameObject.CompareTag("EndGame"))
            {
	            GameEvents.current.TimerEnd();
            }
        }
    }
}