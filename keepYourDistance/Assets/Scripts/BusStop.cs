﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class BusStop : MonoBehaviour
    {
        public Animator animator;
        public string sceneName;
        public GameObject textbubble;
        private bool canEndGame = false;

        private void Start()
        {
            GameEvents.current.TicketCollect += hasTicket;
        }

        private void hasTicket()
        {
            canEndGame = true;
        }
        //if the player enters the bus area, we check if the bus ticket was collected first.
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (canEndGame)
            { //everything applies? Roll the bus animation and turn the player + txtbubble "invisible"
                animator.SetTrigger("BusGo");
                other.gameObject.SetActive(false);
                textbubble.gameObject.SetActive(false);
            }
            else
            { //no ticket ? Activate the bubble that says the ticket is mandatory to enter the bus
                textbubble.gameObject.SetActive(true);
            }
        }

     public void LoadScene()
        {
            SceneManager.LoadScene(sceneName); 
        }
    }
}

