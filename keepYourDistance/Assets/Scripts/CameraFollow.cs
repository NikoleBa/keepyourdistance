﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
public class CameraFollow : MonoBehaviour
{
        public Transform playerTransform;
        //wenn wir den Spieler nicht genau in der Mitte haben wolen, verändern wir diese Variablen
        public float offsetX;
        public float offsetY;

    void LateUpdate()
    {
            //here we store the current Cameraposition
            Vector3 tempCameraPosition = transform.position;
            //das sorgt dafür, dass die Kamera dem Player mit einem gewissen abstand (offset) folgt
            tempCameraPosition.x = playerTransform.position.x + offsetX;
            tempCameraPosition.y = playerTransform.position.y + offsetY;

            transform.position = tempCameraPosition;
    }
}
}
