﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CoATwoPR;

namespace PRTwo
{
    public class GameEvents : MonoBehaviour
    {
        public static GameEvents current;

        void Awake()
        {
            current = this;
        }

        public event Action ToiletPaperCollect;
        public void ToiletPaperHighscore()
        {
            if (ToiletPaperCollect != null)
            {
                ToiletPaperCollect();
            }
        }

        public event Action DoritoCollect;
        public void DoritoHealth()
        {
            if (DoritoCollect != null)
            {
                DoritoCollect();
            }
        }

        public event Action SyringeCollect;
        public void Invulnerability()
        {
            if (SyringeCollect != null)
            {
                SyringeCollect();
            }
        }

        public event Action SoapCollect;
        public void SoapAmmunition()
        {
            if (SoapCollect != null)
            {
                SoapCollect();
            }
        }

        public event Action TicketCollect;
        public void TicketKey()
        {
            if (TicketCollect != null)
            {
                TicketCollect();
            }
        }

        public event Action DamageReceived;
        public void Damaged()
        {
            if (DamageReceived != null)
            {
                DamageReceived();
            }
        }

        public event Action<int> EnemyDefeated;
        public void EnemyKilled(int score)
        {
            if (EnemyDefeated != null)
            {
                EnemyDefeated(score);
            }
        }

        public event Action EnemyHit;
        public void SoapHit()
        {
            if (EnemyHit != null)
            {
                EnemyHit();
            }
        }

        public event Action EnemyMissed;

        public void SoapMissed()
        {
            if (EnemyMissed != null)
            {
                EnemyMissed();
            }
        }

        public event Action Sniper;

        public void ConsecutiveHits()
        {
            if (Sniper != null)
            {
                Sniper();
            }
        }

        public event Action Collector;
        public void TPCollector()
        {
            if (Collector != null)
            {
                Collector();
            }
        }

        public event Action Bunny;
        public void BunnyHopper()
        {
            if (Bunny != null)
            {
                Bunny();
            }
        }

        public event Action KingOfTheWorld;
        public void HighestPoint()
        {
            if (KingOfTheWorld != null)
            {
                KingOfTheWorld();
            }
        }

<<<<<<< HEAD
    public event Action LevelEnded;
    public void LoadNewLevel()
    {
        if (LevelEnded != null)
=======
        public event Action PauseTimer;
        public void TimerStopped()
        {
            if (PauseTimer != null)
            {
                PauseTimer();
            }
        }

        public event Action LevelEnded;
        public void LoadNewLevel()
>>>>>>> develop
        {
            if (LevelEnded != null)
            {
                LevelEnded();
            }
        }

        public event Action TimerOver;

        public void TimerEnd()
        {
            if (TimerOver != null)
            {
                TimerOver();
            }
        }
    }
}
