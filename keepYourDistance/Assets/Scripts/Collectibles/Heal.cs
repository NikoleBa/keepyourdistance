﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Heal : MonoBehaviour
    {
        public AudioSource crunch;

        private void OnTriggerEnter2D(Collider2D other)
        {
            //if the player enters the Heal item trigger, health gets added and a sound plays.
            //heal items are one- use only, so destroy it!
            if (other.gameObject.CompareTag("Player"))
            {//always a full heal
                GameEvents.current.DoritoHealth();
                crunch.Play();
                Destroy(this.gameObject);
            }
        }

    }
}