﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Barrier : MonoBehaviour
    {
        //holds the sprite of the barrier+ animation to visualize the invunerability powerup
        public GameObject barrier;

        public void ActivateBarrier()
        {
            barrier.SetActive(true);
        }

        public void DeactivateBarrier()
        {
            barrier.SetActive(false);
        }
    }
}