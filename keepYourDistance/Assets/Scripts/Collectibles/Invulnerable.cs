﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRTwo
{
    public class Invulnerable : MonoBehaviour
    {
        private GameObject player;
        public AudioSource syringeSound;

        private void Start()
        {
            player = GameObject.Find("Player");
        }

        //when the player collects the item, the coroutine for invulnerability starts
        void OnTriggerEnter2D (Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GameEvents.current.Invulnerability();
                GetComponentInChildren<SpriteRenderer>().enabled = false;
                GetComponent<CircleCollider2D>().enabled = false;
                StartCoroutine("GetInvulnerable");
                syringeSound.Play();
            }
                          
        }
        //Coroutine in which collision layers between player and enemies get disabled for a set time
        IEnumerator GetInvulnerable()
        {
            Physics2D.IgnoreLayerCollision(9,10, true);
            player.GetComponent<Barrier>().ActivateBarrier();
            yield return new WaitForSeconds(5f);
            Physics2D.IgnoreLayerCollision(9,10, false);
            player.GetComponent<Barrier>().DeactivateBarrier();

            // items are one-use only !
            Destroy(this.gameObject);
        }
    }
}