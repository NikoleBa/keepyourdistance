﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

<<<<<<< HEAD
namespace CoATwoPR
=======
namespace PRTwo
{
public class Achievements : MonoBehaviour
>>>>>>> develop
{
    public class Achievements : MonoBehaviour
    {
        public Timer timerScript;
        public GameObject PanelCollector;
        public GameObject PanelBullseye;
        public GameObject PanelBunny;
        public GameObject PanelSpeedrunner;
        public GameObject PanelKing;
        
        private void Start()
        {
            GameEvents.current.Sniper += SniperAchievement;
            GameEvents.current.Collector += CollectorAchievement;
            GameEvents.current.Bunny += BunnyAchievement;
            GameEvents.current.KingOfTheWorld += KingOfTheWorldAchievement;
            GameEvents.current.TimerOver += SpeedrunAchievement;
        }
    
        IEnumerator AchievementDisable()
        {
            yield return new WaitForSeconds(3f);
            PanelBullseye.SetActive(false);
            PanelCollector.SetActive(false);
            PanelBunny.SetActive(false);
            PanelKing.SetActive(false);
        }
    
        private void SniperAchievement()
        {
            PanelBullseye.SetActive(true);
            StartCoroutine(AchievementDisable());
        }
    
        private void CollectorAchievement()
        {
            PanelCollector.SetActive(true);
            StartCoroutine(AchievementDisable());
        }
    
        private void BunnyAchievement()
        {
            PanelBunny.SetActive(true);
            StartCoroutine(AchievementDisable());
        }
    
        private void KingOfTheWorldAchievement()
        {
            PanelKing.SetActive(true);
            StartCoroutine(AchievementDisable());
        }
    
        private void SpeedrunAchievement()
        {
            if (timerScript.timer <= 240f)
            {
                PanelSpeedrunner.SetActive(true);
                StartCoroutine(AchievementDisable());
            }
            else
            {
                return;
            }
        }
    }
<<<<<<< HEAD
}
=======
}
}
>>>>>>> develop
