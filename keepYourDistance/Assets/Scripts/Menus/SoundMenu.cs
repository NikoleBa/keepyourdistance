﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace PRTwo
{
    public class SoundMenu : MonoBehaviour
    {
        //als referenz zur audio
        public AudioMixer audioMixer;
        //um die audio an den slider anzupassen
        public void SetVolume(float volume)
        {
            audioMixer.SetFloat("volume", volume);
        }
    }
}