﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PRTwo
{
    public class MainMenu : MonoBehaviour
    {
        public AudioSource bgm;
       // public GameObject credits;

        void Start()
        {
            
            HighScore.highScore = PlayerPrefs.GetInt("HighScore");
            bgm.Play();
        }

        //läd die Szene Intro
        public void PlayGame()
        {
            SceneManager.LoadScene("Intro");
        }
        //ein Canvas wird aktiviert
        public void loadingCredits()
        {
           // credits.SetActive(true);
        }

        //der Canvas wird deaktiviert
        public void unloadingCredits()
        {
            // credits.SetActive(false);
        }

        //Beendet das Spiel
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}