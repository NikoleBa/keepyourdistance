﻿using System.IO;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.UI;

namespace PRTwo
{
    public class HighScore : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI textMesh;

        //static to keep value over scene changes
        [SerializeField]
        public static int highScore = 0;

        private int score;

        //helper variable to indicate if highscore should be reset to 0
        public bool resetHighscore = false;

        private void Update()
        {
            // textMesh displays the currentHighScore
            textMesh.text = GetHighscoreValue();
        }

        private void Start()
        {
            //Subscription to the Event
            GameEvents.current.ToiletPaperCollect += ToiletPaperHighscoreUp;
            GameEvents.current.DoritoCollect += DoritoHighscoreUp;
            GameEvents.current.SyringeCollect += SyringeHighscoreUp;
            GameEvents.current.SoapCollect += SoapHighscoreUp;
            GameEvents.current.DamageReceived += DamageHighscoreDown;
            GameEvents.current.EnemyDefeated += EnemyDefeatedHighscoreUp;
            GameEvents.current.Sniper += SniperHighscoreUp;
            GameEvents.current.Collector += CollectorHighscoreUp;
            GameEvents.current.Bunny += BunnyHighscoreUp;
            GameEvents.current.KingOfTheWorld += HighestPointHighscoreUp;

            //we reset the Highscore every time we enter Level_01
            if (resetHighscore)
            {
                highScore = 0;
            }
        }
        private void ToiletPaperHighscoreUp()
        {
            highScore += 20;
        }
        
        private void SyringeHighscoreUp()
        {
            highScore += 5;
        }

        private void DoritoHighscoreUp()
        {
            highScore += 5;
        }

        private void SoapHighscoreUp()
        {
            highScore += 5;
        }

        private void DamageHighscoreDown()
        {
            highScore -= 10;
        }

        private void EnemyDefeatedHighscoreUp(int score)
        {
            highScore += score;
        }

        private void SniperHighscoreUp()
        {
            highScore += 250;
        }

        private void CollectorHighscoreUp()
        {
            highScore += 250;
        }

        private void BunnyHighscoreUp()
        {
            highScore += 250;
        }

        private void HighestPointHighscoreUp()
        {
            highScore += 250;
        }
        
        //returns the highscore value to display(can be overridden by child classes)
        protected virtual string GetHighscoreValue()
        {
            return highScore.ToString();
        }
        //add points to the score
        public void AddHighScore(int score)
        {
            highScore += score;
        }
        //saves the current score to player prefs, if the value is higher than that from the player prefs
        public void SaveHighScore()
        {
            if (highScore > PlayerPrefs.GetInt("HighScore", 0))
            {
                PlayerPrefs.SetInt("HighScore", highScore);
                textMesh.text = highScore.ToString();
            }
        }
    }
}