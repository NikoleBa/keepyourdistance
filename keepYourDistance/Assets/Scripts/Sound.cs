﻿using UnityEngine.Audio;
using UnityEngine;

namespace PRTwo
{
    public class Sound
    {
        //Name
        public string name;
        //Clip referenz
        public AudioClip clip;
        //lautstärkeregeler
        [Range(0f, 1f)]
        public float volume;
        //pitch regler
        [Range(.1f, 3f)]
        public float pitch;
        //AudioSource referenz
        [HideInInspector]
        public AudioSource source;
    }
}